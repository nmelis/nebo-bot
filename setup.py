import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="nebo bot",
    version="1.0.0",
    author="NMelis",
    author_email="nebo.game-bots@mail.space",
    description="This module makes to automatic game actions nebo.mobi",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/nmelis/nebo-bot",
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': ['nebo=nebo_bot.command_line:run'],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)