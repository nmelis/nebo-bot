

# just work
FINISHED_GET_COSTS = -10
FINISHED_GET_DELIVERY = -11
FINISHED_GET_BUY_PRODUCTS = -12

# lift
LIFT_EMPTY = -20
FINISHED_LIFT_WORK = -21

# invite users on city
GAVE_POST = -111
DOES_NOT_GAVE_POST = -112
PERMISSION_ERROR_FOR_INITE = -1
CANCEL_INVITING = -2
SUCCESS_FINISHED_INVITE = -3

# other
OPEN_ALL_NEW_FLOORS = -1001
BUY_NEW_FLOOR = -1002
BUILD_NEW_FLOOR = -1003
DOES_NOT_EMPTY_BUILDING_FOR_BUILD = -1004
NOT_BUILD = -1005
NOT_BUY_BUILD = -1006

# Quests
GET_AWARD = -2001
EMPTY_AWARD = -2002

# Works
FINISHED_RUN_WORKS = -500

# humane
ALL_SET_TO_WORK = -200
HOTEL_EMPTY = -202
