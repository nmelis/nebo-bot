import logging

TIMEOUT = 25
DEBUG = True

logging.basicConfig(level=logging.DEBUG if DEBUG else logging.INFO)
